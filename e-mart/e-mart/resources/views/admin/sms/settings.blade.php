@extends('admin.layouts.master')
@section('title','SMS Settings | ')
@section('body')
<div class="box">
    <div class="box-header">
        <div class="box-title">
            MSG91 SMS Settings
        </div>

       
    </div>

    <div class="box-body">

        <div class="callout callout-success">
            <i class="fa fa-info-circle"></i> Important note:

            <ul>
                <li>MSG91 Only send SMS if user did not opt for DND Services.</li>
                <li>
                    If msg not delivering to customer than make sure he/she updated phonecode in his/her profile.
                </li>
            </ul>

            
        </div>

        <form action="{{ route('sms.update.settings') }}" method="POST">
            @csrf

            <div class="row">

                <div class="col-md-12">
                    <div class="form-group eyeCy">
                        <label>MSG91 Auth Key: <span class="text-red">*</span></label>
                        <input id="MSG91_AUTH_KEY" type="password" class="form-control"
                            value="{{ env('MSG91_AUTH_KEY') }}" name="MSG91_AUTH_KEY">
                        <span toggle="#MSG91_AUTH_KEY" class="eye fa fa-fw fa-eye field-icon toggle-password"></span>
                    </div>
                </div>

            </div>



            @foreach ($settings as $row)
            <h4>{{ucfirst( $row->key) }} SMS Settings:</h4>
            <hr>

            <input type="hidden" name="keys[{{ $row->id }}]" value="{{ $row->key }}">

            <div class="row">
                @if($row->key != 'orders')
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Message Text (MUST WITH PLACEHOLDER ##OTP##)<span class="text-red">*</span>:</label>
                        <input placeholder="eg. Your OTP code for login is ##OTP##" type="text" min="1" max="60"
                            class="form-control" value="{{ $row->message ? $row->message : "" }}" name="message[{{ $row->id }}]">
                    </div>
                </div>
                @endif

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Enter SENDER ID: (Max char length 6) <span class="text-red">*</span></label>
                        <input placeholder="eg. SMSIND" maxlength="6" type="text" class="form-control" value="{{ $row->sender_id ? $row->sender_id : "" }}"
                            name="sender_id[{{ $row->id }}]">
                    </div>
                </div>

               @if($row->key != 'orders')

               <div class="col-md-4">
                    <div class="form-group">
                        <label>MSG91 OTP Code Expiry (In Minutes): <span class="text-red">*</span></label>
                        <input type="text" class="form-control" value="{{ $row->otp_expiry ? $row->otp_expiry : "" }}" name="otp_expiry[{{ $row->id }}]">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>MSG91 OTP Code Length (Max:6): <span class="text-red">*</span></label>
                        <input type="number" min="4" max="6" class="form-control" value="{{ $row->otp_length ? $row->otp_length : 4 }}" name="otp_length[{{ $row->id }}]">
                    </div>
                </div>

               @endif

               <div class="col-md-4">

                    <div class="form-group eyeCy">
                        <label>MSG91 Flow ID: <span class="text-red">*</span></label>
                        <input id="flow_id" type="password" class="form-control"
                            value="{{ $row->flow_id }}" name="flow_id[{{$row->id}}]">
                        <span toggle="#flow_id" class="eye fa fa-fw fa-eye field-icon toggle-password"></span>
                    </div>

               </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Enable emoji in Msg: <span class="text-red">*</span></label>
                        <br>
                        <label class="switch">
                            <input id="login_unicode" {{ $row->unicode == 1 ? "checked" : "" }} type="checkbox" name="unicode[{{ $row->id }}]">
                            <span class="knob"></span>
                        </label>
                    </div>
                </div>
                
                
            </div>
            @endforeach

            <div class="form-group">
                <label for="">Enable MSG91 </label>
                <br>
                <label class="switch">
                    <input id="msg91_enable" {{ $configs->msg91_enable == 1 ? "checked" : "" }} type="checkbox" name="msg91_enable">
                    <span class="knob"></span>
                </label>
                <br>
                <small class="text-muted">
                    <i class="fa fa-question-circle"></i> Toggle to activate the MSG-91.
                </small>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-md btn-primary">
                    <i class="fa fa-save"></i> Save Settings
                </button>
            </div>
        </form>
    </div>
</div>
@endsection