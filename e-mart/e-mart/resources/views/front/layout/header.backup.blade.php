<!-- ============================================== HEADER ============================================== -->
  <header class="header-style-1">
    <!-- ============================================== TOP MENU ============================================== -->
    <div class="top-bar animate-dropdown top-main-block-one">
      <div class="container-fluid">
        <div class="header-top-inner">
          <div class="cnt-account">
            <div class="display-none-block">
              <ul class="list-unstyled">

                @if(Auth::check())

                <li class="dropdown notifications-menu">
                  <a title="{{ __('staticwords.notification') }}" href="#" class="dropdown-toggle"
                    data-toggle="dropdown">
                    <i class="fa fa-bell"></i>
                    @if(auth()->user()->unreadnotifications->count())
                    @if(auth()->user()->unreadnotifications->where('n_type','!=','order_v')->count()>0)
                    <sup id="countNoti" class="bell-badge">
                      {{ auth()->user()->unreadnotifications->where('n_type','!=','order_v')->count() }}
                    </sup>
                    @endif
                    @endif
                  </a>



                  <ul id="dropdown" class="z-index99 dropdown-menu">
                    <li class="notiheadergrey header">
                      @if(auth()->user()->unreadnotifications->where('n_type','!=','order_v')->count())
                      {{ __('staticwords.Youhave') }}
                      <b>{{ auth()->user()->unreadnotifications->where('n_type','!=','order_v')->count() }}</b>
                      {{ __('staticwords.notification') }} !
                      <a class="color111 float-right"
                        href="{{ route('clearall') }}">{{ __('staticwords.MarkallasRead') }}</a>
                      @else
                      <span class="text-center">{{ __('staticwords.NoNotifications') }}</span>
                      @endif
                    </li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu notification-menu">
                      @if(auth()->user()->unreadnotifications->count())

                      @foreach(auth()->user()->unreadNotifications as $notification)
                      @if($notification->n_type != "order_v")
                      <li class="notiheaderlightgrey hey1" id="{{ $notification->id }}"
                        onclick="markread('{{ $notification->id }}')">
                        <p></p>
                        <small class="padding5P float-right"><i class="fa fa-clock-o" aria-hidden="true"></i>
                          {{ date('jS M y',strtotime($notification->created_at)) }}</small>
                        <a class="font-weight600 color111" @if($notification->n_type == "order")
                          @foreach(App\Order::where('order_id','=',$notification->url)->get() as $order)
                          href="{{ url('view/order/'.$notification->url) }}"
                          @endforeach
                          @else
                          href="{{ url('mytickets') }}"
                          @endif

                          onclick="markread('{{ $notification->id }}')"><i class="fa fa-circle-o"
                            aria-hidden="true"></i>
                          {{ $notification->data['data'] }}</a>

                        <p></p>

                      </li>

                      @endif

                      @endforeach
                      @endif
                    </ul>
                  </ul>
                </li>

                @if(Auth::user()->role_id == "a")
                <li class="first"><a target="_blank" title="GO to Admin Panel" href="{{route('admin.main')}}"
                    title="Admin">Admin</a></li>
                @elseif(Auth::user()->role_id == 'v')
                @if(isset(Auth::user()->store))
                <li class="first"><a target="_blank" title="{{ __('staticwords.SellerDashboard') }}"
                    href="{{route('seller.dboard')}}" title="Admin">{{ __('staticwords.SellerDashboard') }}</a>
                </li>
                @endif
                @endif
                <li class="myaccount"><a href="{{url('profile')}}"
                    title="My Account"><span>{{ __('staticwords.MyAccount') }}</span></a></li>
                @inject('wish','App\Wishlist')
                @php
                $data = $wish->where('user_id',Auth::user()->id)->get();
                $count = [];

                foreach ($data as $key => $var) {

                  if(isset($var->variant->products) && isset($var->variant)){
                    if($var->variant->products->status == '1'){
                      $count[] = $var->id;
                    }
                  }

                }

                $wishcount = count($count);
                @endphp
                <li class="wishlist"><a href="{{url('wishlist')}}"
                    title="Wishlist"><span>{{ __('staticwords.Wishlist') }}(<span
                        id="wishcount">{{$wishcount}}</span>)</a></li>
                @endif
                @if(Auth::check())
                <li class="login">

                  <a role="button" onclick="logout()">
                    {{ __('staticwords.Logout') }}
                  </a>

                  <form action="{{ route('logout') }}" method="POST" class="logout-form display-none">
                    {{ csrf_field() }}
                  </form>

                </li>
                @else

                <li class="login animate-dropdown-one">
                  <a href="{{url('login')}}" title="Login">
                    <span>
                      {{ __('staticwords.Login') }}
                    </span>
                  </a>
                </li>
                <li class="myaccount"><a href="{{url('register')}}" title="Register"><span>
                      {{ __('staticwords.Register') }}
                    </span></a></li>
                @endif
                <li><a title="Your Comparison list" href="{{ route('compare.list') }}">
                    {{ __('staticwords.Compare') }}
                    @if(Session::has('comparison'))
                    ({{ count(Session::get('comparison')) }})
                    @else
                    (0)
                    @endif
                  </a></li>
                @auth
                <li class="check"><a data-toggle="modal" href="#feed"
                    title="Feedback"><span>{{ __('staticwords.Feedback') }}</span></a></li>

                <li><a href="{{ route('hdesk') }}" title="Help Desk & Support">{{ __('staticwords.hpd') }}</a></li>

                <!-- Feedback Modal -->
                <div data-backdrop="static" data-keyboard="false" class="z-index99 modal fade" id="feed" tabindex="-1"
                  role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="feed-head modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title" id="myModalLabel"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                          {{ __('staticwords.FeedBackUs') }} </h5>
                      </div>
                      <div class="modal-body">
                        <div class="info-feed alert bg-yellow">
                          <i class="fa fa-info-circle"></i>&nbsp;{{ __('staticwords.feedline') }}
                        </div>
                        <form class="needs-validation" action="{{ route('send.feedback') }}" method="POST" novalidate>
                          {{ csrf_field() }}
                          <div class="form-group">
                            <label class="font-weight-bold" for="">{{ __('staticwords.Name') }}: <span
                                class="required">*</span></label>
                            <input required="" type="text" name="name" class="form-control" value="{{ $auth->name }}">
                          </div>
                          <div class="form-group">
                            <label class="font-weight-bold" for="">{{ __('staticwords.Email') }}: <span
                                class="required">*</span></label>
                            <input required="" type="email" name="email" class="form-control"
                              value="{{ $auth->email }}">
                          </div>
                          <div class="form-group">
                            <label class="font-weight-bold" for="">{{ __('staticwords.Message') }}: <span
                                class="required">*</span></label>
                            <textarea required name="msg"
                              placeholder="Tell us What You Like about us? or What should we do to more to improve our portal."
                              cols="30" rows="10" class="form-control"></textarea>
                          </div>
                          <div class="rat">
                            <label class="font-weight-bold">&nbsp;{{ __('staticwords.RateUs') }}: <span
                                class="required">*</span></label>
                            <ul id="starRating" data-stars="5">
                            </ul>
                            <input type="hidden" id="" name="rate" value="1" class="getStar">
                          </div>
                          <button type="submit" class="btn btn-primary">
                            {{ __('staticwords.Send') }}
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                @endauth
              </ul>
            </div>
          </div>
          <!-- /.cnt-account -->

          <div class="cnt-block">
            <ul class="list-unstyled list-inline">

              @php
              $auto = App\AutoDetectGeo::first();
              @endphp

              @if($auto->currency_by_country == 1)

              @php
              //if manual currency by country is enable//
              $myip = $_SERVER['REMOTE_ADDR'];
              $ip = geoip()->getLocation($myip);
              $findcountry = App\Allcountry::where('iso',$ip->iso_code)->first();
              $location = App\Location::all();
              $countryArray = array();
              $manualcurrency = array();

              foreach ($location as $value) {
              $s = explode(',', $value->country_id);

              foreach ($s as $a) {

              if ($a == $findcountry->id) {
              array_push($countryArray, $value);
              }

              }

              }

              foreach ($countryArray as $cid) {
              $c = App\multiCurrency::where('id',$cid->multi_currency)->first();
              array_push($manualcurrency, $c);
              }


              @endphp

              @endif





              @if($auto->enabel_multicurrency == '1')
              <select name="currency" onchange="val()" id="currency">

                @if($auto->currency_by_country == 1)
                @if(!empty($manualcurrency))
                @foreach($manualcurrency as $currency)

                @if(isset($currency->currency))

                <option {{ Session::get('currency')['mainid'] == $currency->currency->id ? "selected" : "" }}
                  value="{{ $currency->currency->id }}">{{ $currency->currency->code }}
                </option>

                @endif

                @endforeach
                @else
                <option value="{{ $defCurrency->currency->id }}">{{ $defCurrency->currency->code }}</option>
                @endif
                @else

                @foreach(App\multiCurrency::all() as $currency)
                <option {{ Session::get('currency')['mainid'] == $currency->currency->id ? "selected" : "" }}
                  value="{{ $currency->currency->id }}">{{ $currency->currency->code }}
                </option>
                @endforeach

                @endif

              </select>
              @else

              @php
              $currency = App\multiCurrency::firstWhere('default_currency','1');
              @endphp

              <select name="currency" onchange="val()" id="currency">

                <option value="{{ $currency->currency->id }}">{{ $currency->currency->code }}
                </option>

              </select>

              @endif

              <select class="changed_language" name="" id="changed_lng">
                @foreach(\DB::table('locales')->where('status','=',1)->get() as $lang)
                <option {{ Session::get('changed_language') == $lang->lang_code ? "selected" : ""}}
                  value="{{ $lang->lang_code }}">{{ $lang->name }}</option>
                @endforeach
              </select>
            </ul>
            <!-- /.list-unstyled -->
          </div>
          <!-- /.cnt-cart -->
          <div class="clearfix"></div>
        </div>
        <!-- /.header-top-inner -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.header-top -->
    <!-- ============================================== TOP MENU : END ============================================== -->
    <div class="main-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-6  col-md-2 col-sm-2 col-lg-2 logo-holder">
            <!-- ============ LOGO ========================================= -->
            <div class="logo"> <a href="{{url('/')}}" title="{{$title}}"> <img height="50px"
                  src="{{url('images/genral/'.$front_logo)}}" alt="logo"> </a> </div>
            <!-- /.logo -->
            <!--=================== LOGO : END ================= -->
          </div>
          <!-- /.logo-holder -->

          <div class="col-lg-7 col-md-7 col-sm-7 col-12 top-search-holder">
            <!-- ====================== SEARCH AREA ======================== -->
            <div class="search-area">

              <form method="get" enctype="multipart/form-data" action="{{url('search/')}}">

                <div class="control-group search-cat-box">

                  <div class="input-group">
                    <span class="input-group-btn">
                      <select id="searchDropMenu" class="" name="cat">
                        <option value="all">{{ __('staticwords.AllCategory') }}</option>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                        @foreach(App\Category::orderBy('id','desc')->select('id','title')->where('status','1')->get(); as $cat)
                          <option value="{{$cat->id}}">{{$cat->title}}</option>
                        @endforeach
                      </select>
                    </span>
                    <input required="" class="search-field" value="" placeholder="{{ __('staticwords.search') }}"
                      name="keyword">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="submit">
                        <i class="fa fa-search"></i>
                      </button>
                    </span>
                  </div>
                  <!-- <button class="search-button"></button> -->
                </div>

              </form>

            </div>
            <!-- ============================= SEARCH AREA : END ============================ -->
          </div>


          <!-- /.top-search-holder -->
          <div class="col-lg-3 col-md-3 col-sm-3 col-0 animate-dropdown top-cart-row">

            <!-- ==================== SHOPPING CART DROPDOWN ============================================================= -->

            

            <div class="dropdown dropdown-cart dropdown-cart-one">

              <a href="{{ url('/cart') }}" class="lnk-cart">
                <div class="items-cart-inner">
                  <div class="basket">
                    <div class="basket-item-count">
                      <span class="count">

                        @if(!empty($auth)) 
                        
                          {{ Auth::user()->cart->count() }}

                        @else

                        @php
                          $c = array();
                          $c = Session::get('cart');
                        if(!empty($c)){
                          $c = array_filter($c);
                        }else{
                          $c = [];
                        }

                        @endphp

                        {{ count($c) }}
                        @endif
                      </span></div>
                    <div class="total-price-basket">
                      <span class="lbl">{{ __('staticwords.Yourcart') }}</span>
                      <?php
                          $total = 0;
                          if(Auth::check()){

                            $carts = Auth::user()->cart;

                            foreach($carts as $key=>$val){

                                if($val->semi_total != null && $val->semi_total != 0 && $val->semi_total != ''){
                                    $price = $val->semi_total;
                                }else{
                                    $price = $val->price_total;
                                }
                                
                                $total =  sprintf("%.2f",$total+$price);
                            } 

                          }
                          else{

                            if(!empty(session()->get('cart'))){
                              
                                    foreach(session()->get('cart') as $key => $val){

                                      if($val['varofferprice'] != 0){
                                        $price = $val['qty']*$val['varofferprice'];
                                      }else{
                                        $price = $val['qty']*$val['varprice'];
                                      }

                                      $total =  sprintf("%.2f",$total+$price);
                                    
                                    } 

                            }
                        }
      
                            ?>
                      @if(Auth::check())
                     
                      @foreach(Auth::user()->cart as $key=>$cart)

                         @php
                             $shipping = ShippingPrice::calculateShipping($cart);
                         @endphp

                      @endforeach

                      @else

                      

                      @if(!empty(session()->get('cart')))

                              @foreach(session()->get('cart') as $key=>$cart)

                              

                                @php
                                  $shipping = 0;
                                  $pros = App\Product::where('id','=',$cart['pro_id'])->first();
                                  $variant = App\AddSubVariant::withTrashed()->where('id','=',$cart['variantid'])->first();
                                  $free_shipping =App\Shipping::where('id',$pros->shipping_id)->first();
                                @endphp

                              @if($pros->free_shipping==0)

                                 @php
                                     $shipping = GuestCartShipping::shipping($variant,$cart);
                                 @endphp
                                 
                              @endif

                      @endforeach

                      @endif

                      @endif


                      @if(Session::get('currency')['position']== 'l' || Session::get('currency')['position'] == 'ls')
                        <i class="{{session()->get('currency')['value']}}"></i>
                      @endif
                      @if(Session::get('currency')['position'] == 'ls')
                        &nbsp;
                      @endif
                      <span class="value" id="total_cart">
                        @if(Session::has('coupanapplied'))

                        {{  sprintf("%.2f",(($total-session('coupanapplied')['discount'])*$conversion_rate)) }}

                        @else
                        {{ sprintf("%.2f",($total*$conversion_rate))}}
                        @endif
                      </span>
                      @if(Session::get('currency')['position'] == 'rs')
                      &nbsp;
                      @endif

                      @if(Session::get('currency')['position']== 'r' || Session::get('currency')['position'] == 'rs')
                        <i class="{{session()->get('currency')['value']}}"></i>
                      @endif

                    </div>
                  </div>
                </div>
              </a>

            @auth
           
            <ul class="list-unstyled list-unstyled-one">
              <li class="dropdown notifications-menu">
                <a title="Notification" href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell"></i>
                  @if(auth()->user()->unreadnotifications->count())
                  @if(auth()->user()->unreadnotifications->where('n_type','!=','order_v')->count()>0)
                  <sup id="countNoti" class="bell-badge">
                    {{ auth()->user()->unreadnotifications->where('n_type','!=','order_v')->count() }}
                  </sup>
                  @endif
                  @endif
                </a>



                <ul id="dropdown" class="dropdown-menu">
                  <li class="notiheadergrey header">
                    @if(auth()->user()->unreadnotifications->where('n_type','!=','order_v')->count())
                    {{ __('staticwords.Youhave') }}
                    <b>{{ auth()->user()->unreadnotifications->where('n_type','!=','order_v')->count() }}</b>
                    {{ __('staticwords.notification') }} !
                    <a class="color111 float-right"
                      href="{{ route('clearall') }}">{{ __('staticwords.MarkallasRead') }}</a>
                    @else
                    <span class="text-center">{{ __('staticwords.NoNotifications') }}</span>
                    @endif
                  </li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu notification-menu">
                    @if(auth()->user()->unreadnotifications->count())

                    @foreach(auth()->user()->unreadNotifications as $notification)
                    @if($notification->n_type != "order_v")
                    <li class="hey1 notiheaderlightgrey" id="{{ $notification->id }}"
                      onclick="markread('{{ $notification->id }}')">


                      <small class="padding5P float-right"><i class="fa fa-clock-o" aria-hidden="true"></i>
                        {{ date('jS M y',strtotime($notification->created_at)) }}</small>
                      <a class="color111 font-weight600" @if($notification->n_type == "order")
                        @foreach(App\Order::where('order_id','=',$notification->url)->get() as $order) --}}
                        href="{{ url('view/order/'.$notification->url) }}"
                        @endforeach
                        @else
                        href="{{ url('mytickets') }}"
                        @endif

                        onclick="markread('{{ $notification->id }}')" ><i class="fa fa-circle-o" aria-hidden="true"></i>
                        {{ $notification->data['data'] }}</a>


                    </li>
                    @endif

                    @endforeach
                    @endif
                  </ul>
                </ul>
              </li>
            </ul>
            @else

            <div class="login-block">
              <a href="{{ route('login') }}">{{ __('staticwords.Login') }}</a>
            </div>
            @endauth
            <!-- /.dropdown-cart -->

            <!-- ======================= SHOPPING CART DROPDOWN : END================================ -->
          </div>

          @auth
          @if($wallet_system == 1)

          <div class="dropdown dropdown-cart">

            <a title="My Wallet" href="{{ route('user.wallet.show') }}" class="lnk-cart">

              <div class="items-cart-inner">
                @if($theme_settings && $theme_settings->key == 'pattern2' || $theme_settings->key == 'pattern5')
                  <img style="width: 35px" class="wallet" src="{{ url('images/wallet-black.png') }}" alt="wallet_icon">
                @else
                  <img style="width: 35px" class="wallet" src="{{ url('images/wallet.png') }}" alt="wallet_icon">
                @endif
                <div class="total-price-basket"> <span class="lbl">{{ __('staticwords.Wallet') }}</span>
                  <span class="value">
                    <i class="{{ session()->get('currency')['value'] }}"></i>
                    @if(isset(Auth::user()->wallet) && Auth::user()->wallet->status == 1)

                    {{ sprintf("%.2f",currency(Auth::user()->wallet->balance, $from = $defCurrency->currency->code, $to = session()->get('currency')['id'] , $format = false)) }}

                    @else
                    0.00
                    @endif
                  </span>
                </div>

              </div>
            </a>

          </div>

          @endif
          @endauth

          <!-- /.top-cart-row -->
        </div>
        <!-- /.row -->

      </div>
      <!-- /.container -->

    </div>
    <!-- /.main-header -->

    <!-- ============================================== NAVBAR ============================================== -->
    <div class="header-nav animate-dropdown header-nav-screen">
      <div class="container-fluid corner">
        <div class="yamm navbar navbar-default" role="navigation">

          <div class="nav-bg-class">
            <div class="navbar-collapse collapse display-none" id="mc-horizontal-menu-collapse">
              <div class="nav-outer">
                <ul class="nav navbar-nav">

                  @include('front.layout.topmenu')

                </ul>
                <!-- /.navbar-nav -->
                <div class="clearfix"></div>
              </div>
              <!-- /.nav-outer -->
            </div>
            <!-- /.navbar-collapse -->

          </div>
          <!-- /.nav-bg-class -->
        </div>
        <!-- /.navbar-default -->
      </div>
      <!-- /.container-class -->

    </div>
    <!-- /.header-nav -->
    <!-- ============================================== NAVBAR : END ============================================== -->

    <!-- ============================================== NAVBAR-small-screen : start ============================================== -->

    <div class="header-nav animate-dropdown header-nav-smallscreen">
      <div class="container">
        <div class="yamm navbar navbar-default" role="navigation">
          <div id="b-wrap">
            <div class="mhead">
              <span class="text-light menu-ham cursor-pointer font-size-20" id="myOverlay">
                <i class="fa fa-bars"></i>
              </span>
            </div>
          </div>
          <div class="menu-new">
            <div class="close-menu-new">
              <a href="javascript:void(0)" class="closebtn">&times;</a>
            </div>
            <div class="smallscreen-bg-block">
              <div class="smallscreen-bg-block-heading"><span id="welcomeHeading">{{ __('staticwords.Welcome') }} @auth
                  {{ Auth::user()->name }} @endauth </span> </div>
            </div>
            <div class="sidenav nav-bg-class" onclick="event.stopPropagation();">
              <!-- <div class="nav-bg-class"> -->
              <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                <div class="nav-outer">
                  <div class="nav-heading-one">{{ __('staticwords.Menu') }}</div>
                  <ul class="nav navbar-nav">
                    <!-- small screen nav menu -->
                    @include('front.layout.mobilemenu')
                    <li class="sidebar">
                      <div class="side-menu animate-dropdown outer-bottom-xs">
                        <div class="head">{{ __('staticwords.Categories') }}</div>
                        <?php $home_slider = App\Widgetsetting::where('name','category')->first(); ?>
                        @if(!empty($home_slider))
                        @if($home_slider->home=='1')
                        <nav class="megamenu-horizontal">

                          <ul class="nav">
                            <?php 
                              $price_array = array();
                            ?>
                            @foreach(App\Category::where('status','1')->orderBy('position','ASC')->get(); as $item)

                            @if($item->products->count()>0)
                            @foreach($item->products as $old)

                            @if($genrals_settings->vendor_enable == 1)
                            @foreach($old->subvariants as $orivar)

                            @if($price_login == 0 || Auth::check())
                            @php

                            $convert_price = 0;
                            $show_price = 0;

                            $commision_setting = App\CommissionSetting::first();

                            if($commision_setting->type == "flat"){

                            $commission_amount = $commision_setting->rate;
                            if($commision_setting->p_type == 'f'){

                            if($old->tax_r !=''){
                            $cit = $commission_amount*$old->tax_r/100;
                            $totalprice = $old->vender_price+$orivar->price+$commission_amount+$cit;
                            $totalsaleprice = $old->vender_offer_price + $orivar->price + $commission_amount+$cit;
                            }else{
                            $totalprice = $old->vender_price+$orivar->price+$commission_amount;
                            $totalsaleprice = $old->vender_offer_price + $orivar->price + $commission_amount;
                            }



                            if($old->vender_offer_price == 0){
                            $totalprice;
                            array_push($price_array, $totalprice);
                            }else{
                            $totalsaleprice;
                            $convert_price = $totalsaleprice==''?$totalprice:$totalsaleprice;
                            $show_price = $totalprice;
                            array_push($price_array, $totalsaleprice);

                            }


                            }else{

                            $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                            $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                            $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                            $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                            if($old->vender_offer_price ==0){
                            $bprice = round($buyerprice,2);

                            array_push($price_array, $bprice);
                            }else{
                            $bsprice = round($buyersaleprice,2);
                            $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                            $show_price = $buyerprice;
                            array_push($price_array, $bsprice);

                            }


                            }
                            }else{

                            $comm = App\Commission::where('category_id',$old->category_id)->first();
                            if(isset($comm)){
                            if($comm->type=='f'){

                            if($old->tax_r !=''){
                            $cit =$comm->rate*$old->tax_r/100;
                            $price = $old->vender_price + $comm->rate+$orivar->price+$cit;
                            $offer = $old->vender_offer_price + $comm->rate+$orivar->price+$cit;
                            }else{
                            $price = $old->vender_price + $comm->rate+$orivar->price;
                            $offer = $old->vender_offer_price + $comm->rate+$orivar->price;
                            }



                            $convert_price = $offer==''?$price:$offer;
                            $show_price = $price;

                            if($old->vender_offer_price == 0){

                            array_push($price_array, $price);
                            }else{
                            array_push($price_array, $offer);
                            }



                            }
                            else{

                            $commission_amount = $comm->rate;

                            $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                            $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                            $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                            $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                            if($old->vender_offer_price ==0){
                            $bprice = round($buyerprice,2);
                            array_push($price_array, $bprice);
                            }else{
                            $bsprice = round($buyersaleprice,2);
                            $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                            $show_price = round($buyerprice,2);
                            array_push($price_array, $bsprice);
                            }



                            }
                            }else{
                            $commission_amount = 0;

                            $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                            $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                            $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                            $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                            if($old->vender_offer_price ==0){
                            $bprice = round($buyerprice,2);
                            array_push($price_array, $bprice);
                            }else{
                            $bsprice = round($buyersaleprice,2);
                            $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                            $show_price = round($buyerprice,2);
                            array_push($price_array, $bsprice);
                            }
                            }
                            }

                            @endphp


                            @endif


                            @endforeach
                            @else
                            @if($old->vender['role_id'] == 'a')
                            @foreach($old->subvariants as $orivar)

                            @if($price_login == 0 || Auth::check())
                            @php

                            $convert_price = 0;
                            $show_price = 0;

                            $commision_setting = App\CommissionSetting::first();

                            if($commision_setting->type == "flat"){

                            $commission_amount = $commision_setting->rate;
                            if($commision_setting->p_type == 'f'){

                            if($old->tax_r !=''){
                            $cit = $commission_amount*$old->tax_r/100;
                            $totalprice = $old->vender_price+$orivar->price+$commission_amount+$cit;
                            $totalsaleprice = $old->vender_offer_price + $orivar->price + $commission_amount+$cit;
                            }else{
                            $totalprice = $old->vender_price+$orivar->price+$commission_amount;
                            $totalsaleprice = $old->vender_offer_price + $orivar->price + $commission_amount;
                            }



                            if($old->vender_offer_price == 0){
                            $totalprice;
                            array_push($price_array, $totalprice);
                            }else{
                            $totalsaleprice;
                            $convert_price = $totalsaleprice==''?$totalprice:$totalsaleprice;
                            $show_price = $totalprice;
                            array_push($price_array, $totalsaleprice);

                            }


                            }else{

                            $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                            $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                            $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                            $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                            if($old->vender_offer_price ==0){
                            $bprice = round($buyerprice,2);

                            array_push($price_array, $bprice);
                            }else{
                            $bsprice = round($buyersaleprice,2);
                            $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                            $show_price = $buyerprice;
                            array_push($price_array, $bsprice);

                            }


                            }
                            }else{

                            $comm = App\Commission::where('category_id',$old->category_id)->first();
                            if(isset($comm)){
                            if($comm->type=='f'){

                            if($old->tax_r !=''){
                            $cit =$comm->rate*$old->tax_r/100;
                            $price = $old->vender_price + $comm->rate+$orivar->price+$cit;
                            $offer = $old->vender_offer_price + $comm->rate+$orivar->price+$cit;
                            }else{
                            $price = $old->vender_price + $comm->rate+$orivar->price;
                            $offer = $old->vender_offer_price + $comm->rate+$orivar->price;
                            }



                            $convert_price = $offer==''?$price:$offer;
                            $show_price = $price;

                            if($old->vender_offer_price == 0){

                            array_push($price_array, $price);
                            }else{
                            array_push($price_array, $offer);
                            }



                            }
                            else{

                            $commission_amount = $comm->rate;

                            $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                            $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                            $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                            $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                            if($old->vender_offer_price ==0){
                            $bprice = round($buyerprice,2);
                            array_push($price_array, $bprice);
                            }else{
                            $bsprice = round($buyersaleprice,2);
                            $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                            $show_price = round($buyerprice,2);
                            array_push($price_array, $bsprice);
                            }



                            }
                            }else{
                            $commission_amount = 0;

                            $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                            $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                            $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                            $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                            if($old->vender_offer_price ==0){
                            $bprice = round($buyerprice,2);
                            array_push($price_array, $bprice);
                            }else{
                            $bsprice = round($buyersaleprice,2);
                            $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                            $show_price = round($buyerprice,2);
                            array_push($price_array, $bsprice);
                            }
                            }
                            }

                            @endphp


                            @endif


                            @endforeach
                            @endif
                            @endif

                            @endforeach

                            @if($price_login == 0 || Auth::check())
                            <?php
                                          if($price_array != null){
                                           $first =  min($price_array);
                                           $startp =  round($first);
                                           if($startp >= $first){
                                              $startp = $startp-1;
                                            }else{
                                              $startp = $startp;
                                            }

                                            
                                           $last = max($price_array);
                                           $endp =  round($last);

                                           if($endp <= $last){
                                              $endp = $endp+1;
                                            }else{
                                              $endp = $endp;
                                            }

                                          }
                                          else{
                                            $startp = 0.00;
                                            $endp = 0.00;
                                          }

                                          if(isset($first)){

                                            if($first == $last){
                                              $startp=0.00;
                                            }

                                          }
                                          

                                           unset($price_array); 
                                          
                                          $price_array = array();
                                          ?>

                            @endif




                            <li class="dropdown menu-item" id="dropdown">

                              <a aria-expanded="false" data-id="{{ $item->id }}" title="{{ $item->title }}"
                                class="categoryrec cat-page">
                                <i class="fa {{ $item->icon }}"></i>
                                @if($price_login == 0 || Auth::check())

                                <label class="cursor-pointer"
                                  onclick="categoryfilter('{{$item->id}}','','','{{ $startp }}','{{ $endp }}')">{{$item->title}}</label>

                                @else

                                <label class="cursor-pointer">{{$item->title}}</label>

                                @endif
                              </a>


                              <ul class="display-none" id="childOpen{{ $item->id }}">

                                @foreach($item->subcategory->where('status','1')->sortBy('position') as $s)


                                @foreach($s->products as $old)

                                @if($genrals_settings->vendor_enable == 1)
                                @foreach($old->subvariants as $orivar)

                                @if($price_login== 0 || Auth::check())
                                @php
                                $convert_price = 0;
                                $show_price = 0;

                                $commision_setting = App\CommissionSetting::first();

                                if($commision_setting->type == "flat"){

                                $commission_amount = $commision_setting->rate;
                                if($commision_setting->p_type == 'f'){

                                if($old->tax_r !=''){
                                $cit = $commission_amount*$old->tax_r/100;
                                $totalprice = $old->vender_price+$orivar->price+$commission_amount+$cit;
                                $totalsaleprice = $old->vender_offer_price + $orivar->price + $commission_amount+$cit;
                                }else{
                                $totalprice = $old->vender_price+$orivar->price+$commission_amount;
                                $totalsaleprice = $old->vender_offer_price + $orivar->price + $commission_amount;
                                }

                                if($old->vender_offer_price == 0){
                                $totalprice;
                                array_push($price_array, $totalprice);
                                }else{
                                $totalsaleprice;
                                $convert_price = $totalsaleprice==''?$totalprice:$totalsaleprice;
                                $show_price = $totalprice;
                                array_push($price_array, $totalsaleprice);

                                }


                                }else{

                                $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                if($old->vender_offer_price ==0){
                                $bprice = round($buyerprice,2);

                                array_push($price_array, $bprice);
                                }else{
                                $bsprice = round($buyersaleprice,2);
                                $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                $show_price = $buyerprice;
                                array_push($price_array, $bsprice);

                                }


                                }
                                }else{

                                $comm = App\Commission::where('category_id',$old->category_id)->first();
                                if(isset($comm)){
                                if($comm->type=='f'){

                                if($old->tax_r !=''){
                                $cit =$comm->rate*$old->tax_r/100;
                                $price = $old->vender_price + $comm->rate+$orivar->price+$cit;
                                $offer = $old->vender_offer_price + $comm->rate+$orivar->price+$cit;
                                }else{
                                $price = $old->vender_price + $comm->rate+$orivar->price;
                                $offer = $old->vender_offer_price + $comm->rate+$orivar->price;
                                }

                                $convert_price = $offer==''?$price:$offer;
                                $show_price = $price;

                                if($old->vender_offer_price == 0){

                                array_push($price_array, $price);
                                }else{
                                array_push($price_array, $offer);
                                }



                                }
                                else{

                                $commission_amount = $comm->rate;

                                $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                if($old->vender_offer_price ==0){
                                $bprice = round($buyerprice,2);
                                array_push($price_array, $bprice);
                                }else{
                                $bsprice = round($buyersaleprice,2);
                                $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                $show_price = round($buyerprice,2);
                                array_push($price_array, $bsprice);
                                }



                                }
                                }else{
                                $commission_amount = 0;

                                $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                if($old->vender_offer_price ==0){
                                $bprice = round($buyerprice,2);
                                array_push($price_array, $bprice);
                                }else{
                                $bsprice = round($buyersaleprice,2);
                                $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                $show_price = round($buyerprice,2);
                                array_push($price_array, $bsprice);
                                }
                                }
                                }

                                @endphp


                                @endif


                                @endforeach
                                @else
                                @if($old->vender['role_id'] == 'a')
                                @foreach($old->subvariants as $orivar)

                                @if($price_login== 0 || Auth::check())
                                @php
                                $convert_price = 0;
                                $show_price = 0;

                                $commision_setting = App\CommissionSetting::first();

                                if($commision_setting->type == "flat"){

                                $commission_amount = $commision_setting->rate;
                                if($commision_setting->p_type == 'f'){

                                if($old->tax_r !=''){
                                $cit = $commission_amount*$old->tax_r/100;
                                $totalprice = $old->vender_price+$orivar->price+$commission_amount+$cit;
                                $totalsaleprice = $old->vender_offer_price + $orivar->price + $commission_amount+$cit;
                                }else{
                                $totalprice = $old->vender_price+$orivar->price+$commission_amount;
                                $totalsaleprice = $old->vender_offer_price + $orivar->price + $commission_amount;
                                }

                                if($old->vender_offer_price == 0){
                                $totalprice;
                                array_push($price_array, $totalprice);
                                }else{
                                $totalsaleprice;
                                $convert_price = $totalsaleprice==''?$totalprice:$totalsaleprice;
                                $show_price = $totalprice;
                                array_push($price_array, $totalsaleprice);

                                }


                                }else{

                                $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                if($old->vender_offer_price ==0){
                                $bprice = round($buyerprice,2);

                                array_push($price_array, $bprice);
                                }else{
                                $bsprice = round($buyersaleprice,2);
                                $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                $show_price = $buyerprice;
                                array_push($price_array, $bsprice);

                                }


                                }
                                }else{

                                $comm = App\Commission::where('category_id',$old->id)->first();
                                if(isset($comm)){
                                if($comm->type=='f'){

                                if($old->tax_r !=''){
                                $cit =$comm->rate*$old->tax_r/100;
                                $price = $old->vender_price + $comm->rate+$orivar->price+$cit;
                                $offer = $old->vender_offer_price + $comm->rate+$orivar->price+$cit;
                                }else{
                                $price = $old->vender_price + $comm->rate+$orivar->price;
                                $offer = $old->vender_offer_price + $comm->rate+$orivar->price;
                                }

                                $convert_price = $offer==''?$price:$offer;
                                $show_price = $price;

                                if($old->vender_offer_price == 0){

                                array_push($price_array, $price);
                                }else{
                                array_push($price_array, $offer);
                                }



                                }
                                else{

                                $commission_amount = $comm->rate;

                                $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                if($old->vender_offer_price ==0){
                                $bprice = round($buyerprice,2);
                                array_push($price_array, $bprice);
                                }else{
                                $bsprice = round($buyersaleprice,2);
                                $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                $show_price = round($buyerprice,2);
                                array_push($price_array, $bsprice);
                                }


                                }
                                }else{
                                $commission_amount = 0;

                                $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                if($old->vender_offer_price ==0){
                                $bprice = round($buyerprice,2);
                                array_push($price_array, $bprice);
                                }else{
                                $bsprice = round($buyersaleprice,2);
                                $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                $show_price = round($buyerprice,2);
                                array_push($price_array, $bsprice);
                                }
                                }
                                }

                                @endphp


                                @endif


                                @endforeach
                                @endif
                                @endif

                                @endforeach

                                @if($price_login == 0 || Auth::check())
                                <?php

                                        
                                        if($price_array != null){
                                         $firstsub =  min($price_array);
                                         $startp =  round($firstsub);
                                         if($startp >= $firstsub){
                                            $startp = $startp-1;
                                          }else{
                                            $startp = $startp;
                                          }

                                          
                                         $lastsub = max($price_array);
                                         $endp =  round($lastsub);

                                         if($endp <= $lastsub){
                                            $endp = $endp+1;
                                          }else{
                                            $endp = $endp;
                                          }

                                        }else{
                                          $startp = 0.00;
                                          $endp = 0.00;
                                        }

                                        if(isset($firstsub)){
                                             if($firstsub == $lastsub){
                                                $startp=0.00;
                                            }
                                        }
                                       

                                         unset($price_array); 
                                        
                                        $price_array = array();
                                        ?>
                                @endif
                                <li
                                  class="dropdown menu-item {{ $s->childcategory->count()>0 ? "" : "side-sub-list" }}">
                                  <a class="childrec" title="{{$s->title}}" data-id="{{ $s->id }}"><i
                                      class="fa {{ $s->icon }}"></i>
                                    @if($price_login == 0 || Auth::check())

                                    <label class="cursor-pointer"
                                      onclick="categoryfilter('{{$item->id}}','{{ $s->id }}','','{{ $startp }}','{{ $endp }}')">{{$s->title}}</label>

                                    @else

                                    <label class="cursor-pointer">{{$s->title}}</label>

                                    @endif
                                  </a>

                                  <!-- child category loop -->

                                  @if($s->childcategory->count()>0)

                                  <ul class="display-none" id="childcollapseExample{{ $s->id }}">
                                    @foreach($s->childcategory->where('status','1')->sortBy('position') as $child)

                                    @foreach($child->products as $old)

                                    @if($genrals_settings->vendor_enable == 1)
                                    @foreach($old->subvariants as $orivar)

                                    @if($price_login== 0 || Auth::user())
                                    @php
                                    $convert_price = 0;
                                    $show_price = 0;

                                    $commision_setting = App\CommissionSetting::first();

                                    if($commision_setting->type == "flat"){

                                    $commission_amount = $commision_setting->rate;
                                    if($commision_setting->p_type == 'f'){

                                    if($old->tax_r !=''){
                                    $cit = $commission_amount*$old->tax_r/100;
                                    $totalprice = $old->vender_price+$orivar->price+$commission_amount+$cit;
                                    $totalsaleprice = $old->vender_offer_price + $orivar->price +
                                    $commission_amount+$cit;
                                    }else{
                                    $totalprice = $old->vender_price+$orivar->price+$commission_amount;
                                    $totalsaleprice = $old->vender_offer_price + $orivar->price + $commission_amount;
                                    }

                                    if($old->vender_offer_price == 0){
                                    $totalprice;
                                    array_push($price_array, $totalprice);
                                    }else{
                                    $totalsaleprice;
                                    $convert_price = $totalsaleprice==''?$totalprice:$totalsaleprice;
                                    $show_price = $totalprice;
                                    array_push($price_array, $totalsaleprice);

                                    }


                                    }else{

                                    $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                    $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                    $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                    $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                    if($old->vender_offer_price ==0){
                                    $bprice = round($buyerprice,2);

                                    array_push($price_array, $bprice);
                                    }else{
                                    $bsprice = round($buyersaleprice,2);
                                    $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                    $show_price = $buyerprice;
                                    array_push($price_array, $bsprice);

                                    }


                                    }
                                    }else{

                                    $comm = App\Commission::where('category_id',$old->category_id)->first();
                                    if(isset($comm)){
                                    if($comm->type=='f'){

                                    if($old->tax_r !=''){
                                    $cit =$comm->rate*$old->tax_r/100;
                                    $price = $old->vender_price + $comm->rate+$orivar->price+$cit;
                                    $offer = $old->vender_offer_price + $comm->rate+$orivar->price+$cit;
                                    }else{
                                    $price = $old->vender_price + $comm->rate+$orivar->price;
                                    $offer = $old->vender_offer_price + $comm->rate+$orivar->price;
                                    }

                                    $convert_price = $offer==''?$price:$offer;
                                    $show_price = $price;

                                    if($old->vender_offer_price == 0){

                                    array_push($price_array, $price);
                                    }else{
                                    array_push($price_array, $offer);
                                    }



                                    }
                                    else{

                                    $commission_amount = $comm->rate;

                                    $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                    $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                    $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                    $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                    if($old->vender_offer_price ==0){
                                    $bprice = round($buyerprice,2);
                                    array_push($price_array, $bprice);
                                    }else{
                                    $bsprice = round($buyersaleprice,2);
                                    $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                    $show_price = round($buyerprice,2);
                                    array_push($price_array, $bsprice);
                                    }



                                    }
                                    }else{
                                    $commission_amount = 0;

                                    $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                    $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                    $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                    $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                    if($old->vender_offer_price ==0){
                                    $bprice = round($buyerprice,2);
                                    array_push($price_array, $bprice);
                                    }else{
                                    $bsprice = round($buyersaleprice,2);
                                    $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                    $show_price = round($buyerprice,2);
                                    array_push($price_array, $bsprice);
                                    }
                                    }
                                    }

                                    @endphp


                                    @endif


                                    @endforeach
                                    @else
                                    @if($old->vender['role_id'] == 'a')
                                    @foreach($old->subvariants as $orivar)

                                    @if($price_login== 0 || Auth::user())
                                    @php
                                    $convert_price = 0;
                                    $show_price = 0;

                                    $commision_setting = App\CommissionSetting::first();

                                    if($commision_setting->type == "flat"){

                                    $commission_amount = $commision_setting->rate;
                                    if($commision_setting->p_type == 'f'){

                                    if($old->tax_r !=''){
                                    $cit = $commission_amount*$old->tax_r/100;
                                    $totalprice = $old->vender_price+$orivar->price+$commission_amount+$cit;
                                    $totalsaleprice = $old->vender_offer_price + $orivar->price +
                                    $commission_amount+$cit;
                                    }else{
                                    $totalprice = $old->vender_price+$orivar->price+$commission_amount;
                                    $totalsaleprice = $old->vender_offer_price + $orivar->price + $commission_amount;
                                    }

                                    if($old->vender_offer_price == 0){
                                    $totalprice;
                                    array_push($price_array, $totalprice);
                                    }else{
                                    $totalsaleprice;
                                    $convert_price = $totalsaleprice==''?$totalprice:$totalsaleprice;
                                    $show_price = $totalprice;
                                    array_push($price_array, $totalsaleprice);

                                    }


                                    }else{

                                    $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                    $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                    $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                    $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                    if($old->vender_offer_price ==0){
                                    $bprice = round($buyerprice,2);

                                    array_push($price_array, $bprice);
                                    }else{
                                    $bsprice = round($buyersaleprice,2);
                                    $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                    $show_price = $buyerprice;
                                    array_push($price_array, $bsprice);

                                    }


                                    }
                                    }else{

                                    $comm = App\Commission::where('category_id',$old->category_id)->first();
                                    if(isset($comm)){
                                    if($comm->type=='f'){

                                    if($old->tax_r !=''){
                                    $cit =$comm->rate*$old->tax_r/100;
                                    $price = $old->vender_price + $comm->rate+$orivar->price+$cit;
                                    $offer = $old->vender_offer_price + $comm->rate+$orivar->price+$cit;
                                    }else{
                                    $price = $old->vender_price + $comm->rate+$orivar->price;
                                    $offer = $old->vender_offer_price + $comm->rate+$orivar->price;
                                    }

                                    $convert_price = $offer==''?$price:$offer;
                                    $show_price = $price;

                                    if($old->vender_offer_price == 0){

                                    array_push($price_array, $price);
                                    }else{
                                    array_push($price_array, $offer);
                                    }

                                    }
                                    else{

                                    $commission_amount = $comm->rate;

                                    $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                    $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                    $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                    $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                    if($old->vender_offer_price ==0){
                                    $bprice = round($buyerprice,2);
                                    array_push($price_array, $bprice);
                                    }else{
                                    $bsprice = round($buyersaleprice,2);
                                    $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                    $show_price = round($buyerprice,2);
                                    array_push($price_array, $bsprice);
                                    }


                                    }
                                    }else{
                                    $commission_amount = 0;

                                    $totalprice = ($old->vender_price+$orivar->price)*$commission_amount;

                                    $totalsaleprice = ($old->vender_offer_price+$orivar->price)*$commission_amount;

                                    $buyerprice = ($old->vender_price+$orivar->price)+($totalprice/100);

                                    $buyersaleprice = ($old->vender_offer_price+$orivar->price)+($totalsaleprice/100);


                                    if($old->vender_offer_price ==0){
                                    $bprice = round($buyerprice,2);
                                    array_push($price_array, $bprice);
                                    }else{
                                    $bsprice = round($buyersaleprice,2);
                                    $convert_price = $buyersaleprice==''?$buyerprice:$buyersaleprice;
                                    $show_price = round($buyerprice,2);
                                    array_push($price_array, $bsprice);
                                    }
                                    }
                                    }

                                    @endphp


                                    @endif


                                    @endforeach
                                    @endif
                                    @endif

                                    @endforeach

                                    <?php
                                                      
                                                      if($price_login == 0 || Auth::check()){
                                                          if($price_array != null){
                                                       $first =  min($price_array);
                                                       $startp =  round($first);
                                                       if($startp >= $first){
                                                          $startp = $startp-1;
                                                        }else{
                                                          $startp = $startp;
                                                        }

                                                        
                                                       $last = max($price_array);
                                                       $endp =  round($last);

                                                       if($endp <= $last){
                                                          $endp = $endp+1;
                                                        }else{
                                                          $endp = $endp;
                                                        }

                                                      }else{
                                                        $startp = 0.00;
                                                        $endp = 0.00;
                                                      }

                                                      if(isset($firstsub))
                                                      { 
                                                        if($firstsub == $lastsub){
                                                          $startp=0.00;
                                                      }
                                                      }
                                                      

                                                       unset($price_array); 
                                                        
                                                      
                                                      $price_array = array();
                                                      }
                                                      
                                                      ?>
                                    <li class="left-15 menu-item side-sub-list">
                                      @if($price_login == 0 || Auth::check())
                                      <a title="{{$child->title}}" class="cursor-pointer"
                                        onclick="categoryfilter('{{$item->id}}','{{ $s->id }}','{{ $child->id }}','{{ $startp }}','{{ $endp }}')"><i
                                          class="fa fa-angle-double-right" aria-hidden="true"></i> {{$child->title}}
                                      </a>
                                      @else
                                      <a class="cursor-pointer" title="{{ $child['title'] }}"><i
                                          class="fa fa-angle-double-right" aria-hidden="true"></i>
                                        {{$child['title']}}</a>
                                      @endif
                                    </li>

                                    @endforeach

                                  </ul>

                                  @endif



                                </li>
                                @endforeach
                              </ul>

                              @endif
                              @endforeach
                          </ul>

                          <!-- /.nav -->
                        </nav>
                        @endif
                        @endif
                        <!-- /.megamenu-horizontal -->
                      </div>
                    </li>
                    <li>
                      <div class="module-heading">
                        <h4 class="module-title">{{$footer3_widget->footer2}}</h4>
                      </div>
                      <!-- /.module-heading -->

                      <div class="module-body">
                        <ul class='list-unstyled'>
                          @if(Auth::check())

                          <li class="first"><a href="{{url('profile')}}"
                              title="My Account">{{ __('staticwords.MyAccount') }}</a></li>
                          <li>
                            <a href="{{ route('user.wallet.show') }}">
                              {{ __('staticwords.MyWallet') }}

                              ( <i class="{{session()->get('currency')['value']}}"></i>@if(isset(Auth::user()->wallet)
                              && Auth::user()->wallet->status == 1)

                              {{ sprintf("%.2f",currency(Auth::user()->wallet->balance, $from = $defCurrency->currency->code, $to = session()->get('currency')['id'] , $format = false)) }}

                              @else
                              0.00
                              @endif)
                            </a>
                          </li>
                          <li><a href="{{url('order')}}" title="Order History">{{ __('staticwords.OrderHistory') }}</a>
                          </li>
                          @endif
                          <li><a href="{{url('faq')}}" title="faq">{{ __('staticwords.faqs') }}</a></li>

                        </ul>
                      </div>
                    </li>
                    <li>
                    </li>
                    <li>
                      <div class="module-heading">
                        <h4 class="module-title">{{$footer3_widget->footer3}}</h4>
                      </div>


                      @foreach($footermenus->where('widget_postion','=','footer_wid_3')->where('status','1')->get() as
                      $fm)
                      <div class="module-body">
                        <ul class='list-unstyled'>
                          <li class="first">

                            @if($fm->link_by == 'page')
                            <a title="{{ $fm->title }}" href="{{ route('page.slug',$fm->gotopage['slug']) }}">
                              {{ $fm->title }}
                            </a>
                            @else
                            <a target="__blank" title="{{ $fm->title }}" href="{{ $fm->url }}">
                              {{ $fm->title }}
                            </a>
                            @endif

                          </li>
                        </ul>
                      </div>
                      @endforeach




                    </li>
                    <li>
                      <div class="module-heading">
                        <h4 class="module-title">{{$footer3_widget->footer4}}</h4>
                      </div>

                      @foreach($footermenus->where('widget_postion','=','footer_wid_4')->where('status','1')->get() as
                      $foo)
                      <div class="module-body">
                        <ul class='list-unstyled'>
                          <li class="first">

                            @if($foo->link_by == 'page')
                            <a title="{{ $foo->title }}" href="{{ route('page.slug',$foo->gotopage['slug']) }}">
                              {{ $foo->title }}
                            </a>
                            @else
                            <a target="__blank" title="{{ $foo->title }}" href="{{ $foo->url }}">
                              {{ $foo->title }}
                            </a>
                            @endif

                          </li>
                        </ul>
                      </div>
                      @endforeach

                    </li>
                    <li class="top-bar animate-dropdown">
                      <div class="header-top-inner">
                        <div class="cnt-account">
                          <ul class="list-unstyled">

                            @auth
                            <li class="myaccount"><a href="{{url('profile')}}"
                                title="My Account">{{ __('staticwords.MyAccount') }}</a></li>
                            @inject('wish','App\Wishlist');
                            @php
                              $data = $wish->where('user_id',Auth::user()->id)->get();
                              $count = [];

                              foreach ($data as $key => $var) {

                                  if(isset($var->variant->products) && isset($var->variant)){
                                    if($var->variant->products->status == '1'){
                                      $count[] = $var->id;
                                    }
                                  }

                              }

                              $wishcount = count($count);
                            @endphp
                            <li class="wishlist"><a href="{{url('wishlist')}}" title="Wishlist">
                                {{ __('staticwords.Wishlist') }} ({{ $wishcount }})</a></li>
                            <li class="login">

                              <a role="button" onclick="logout()">
                                {{ __('staticwords.Logout') }}
                              </a>

                              <form action="{{ route('logout') }}" method="POST" class="logout-form display-none">
                                {{ csrf_field() }}
                              </form>
                            </li>

                            <li class="check"><a data-toggle="modal" href="#feedonmobile"
                                title="Feedback">{{ __('staticwords.Feedback') }}</a></li>
                            @endauth
                            <li><a href="{{ route('hdesk') }}" title="Help Desk &amp; Support"><i
                                  class="fa fa-question-circle"></i>{{ __('staticwords.hpd') }}</a></li>
                            <li><a title="{{ __('staticwords.ContactUs') }}" href="{{ route('contact.us') }}"
                                    title="Feedback"><i class="fa fa-phone"></i> {{ __('staticwords.ContactUs') }}</a></li>

                          </ul>
                          <!-- Feedback Modal -->
                          <div data-backdrop="static" data-keyboard="false" class="z-index99 modal fade"
                            id="feedonmobile" tabindex="99" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="feed-head modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                      aria-hidden="true">×</span></button>
                                  <h5 class="modal-title" id="myModalLabel"><i class="fa fa-envelope-o"
                                      aria-hidden="true"></i>
                                    {{ __('staticwords.FeedBackUs') }}
                                  </h5>
                                </div>
                                <div class="modal-body">
                                  <div class="info-feed alert bg-yellow">
                                    <i class="fa fa-info-circle"></i>&nbsp;{{ __('staticwords.feedline') }}.
                                  </div>
                                  <form action="{{ route('send.feedback') }}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                      <label class="font-weight-bold" for="">{{ __('staticwords.Name') }}: <span
                                          class="required">*</span></label>
                                      <input required="" type="text" name="name" class="form-control" value="Admin">
                                    </div>
                                    <div class="form-group">
                                      <label class="font-weight-bold" for="">{{ __('staticwords.Email') }}: <span
                                          class="required">*</span></label>
                                      <input required="" type="email" name="email" class="form-control"
                                        value="info@example.com">
                                    </div>
                                    <div class="form-group">
                                      <label class="font-weight-bold" for="">{{ __('staticwords.Message') }}: <span
                                          class="required">*</span></label>
                                      <textarea required="" name="msg"
                                        placeholder="{{ __('Tell us What You Like about us? or What should we do to more to improve our portal.') }}"
                                        cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                    <div class="rat">
                                      <label class="font-weight-bold">&nbsp;{{ __('staticwords.RateUs') }}: <span
                                          class="required">*</span></label>
                                      <ul id="starRating" data-stars="5" class="starRating starrating-init">
                                        <li class="star"><i class="fa fa-star"></i></li>
                                        <li class="star"><i class="fa fa-star"></i></li>
                                        <li class="star"><i class="fa fa-star"></i></li>
                                        <li class="star"><i class="fa fa-star"></i></li>
                                        <li class="star"><i class="fa fa-star"></i></li>
                                      </ul>
                                      <input type="hidden" id="" name="rate" value="1">
                                    </div>
                                    <button type="submit" class="btn btn-primary">{{ __('staticwords.Send') }}</button>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /.cnt-account -->
                        <div class="cnt-block">
                          <ul class="list-unstyled list-inline">
                            @php
                            $auto = App\AutoDetectGeo::first();
                            @endphp
                            @if($auto->currency_by_country == 1)

                            @php
                            //if manual currency by country is enable//
                            $myip = $_SERVER['REMOTE_ADDR'];
                            $ip = geoip()->getLocation($myip);
                            $findcountry = App\Allcountry::where('iso',$ip->iso_code)->first();
                            $location = App\Location::all();
                            $countryArray = array();
                            $manualcurrency = array();

                            foreach ($location as $value) {
                            $s = explode(',', $value->country_id);

                            foreach ($s as $a) {

                            if ($a == $findcountry->id) {
                            array_push($countryArray, $value);
                            }
                            }

                            }

                            foreach ($countryArray as $cid) {
                            $c = App\multiCurrency::where('id',$cid->multi_currency)->first();
                            array_push($manualcurrency, $c);
                            }

                            @endphp

                            @endif

                            @if($auto->enabel_multicurrency == 1)

                            <select name="currency" onchange="val2()" id="currencySmall">

                              @if($auto->currency_by_country == 1)

                              @if(!empty($manualcurrency))
                              @foreach($manualcurrency as $currency)

                              @if(isset($currency->currency))

                              <option
                                {{ Session::get('currency')['mainid'] == $currency->currency->id ? "selected" : "" }}
                                value="{{ $currency->currency->id }}">{{ $currency->currency->code }}
                              </option>

                              @endif

                              @endforeach
                              @else
                              <option value="{{ $defCurrency->currency->id }}">{{ $defCurrency->currency->code }}
                              </option>
                              @endif

                              @else

                              @foreach(App\multiCurrency::all() as $currency)
                              <option
                                {{ Session::get('currency')['mainid'] == $currency->currency->id ? "selected" : "" }}
                                value="{{ $currency->currency->id }}">{{ $currency->currency->code }}
                              </option>
                              @endforeach

                              @endif

                            </select>

                            @else

                            @php
                            $currency = App\multiCurrency::firstWhere('default_currency','1');
                            @endphp

                            <select name="currency" onchange="val()" id="currency">

                              <option value="{{ $currency->currency->id }}">{{ $currency->currency->code }}
                              </option>

                            </select>

                            @endif

                            <select class="changed_language" name="changed_language" id="changed_lng">
                              @foreach(\DB::table('locales')->where('status','=',1)->get() as $lang)
                              <option {{ Session::get('changed_language') == $lang->lang_code ? "selected" : ""}}
                                value="{{ $lang->lang_code }}">{{ $lang->name }}</option>
                              @endforeach
                            </select>

                          </ul>
                          <!-- /.list-unstyled -->
                        </div>
                        <!-- /.cnt-cart -->
                        <div class="clearfix"></div>
                      </div>
                    </li>
                  </ul>
                  <!-- /.navbar-nav -->
                  <div class="clearfix"></div>
                </div> <!-- /.nav-outer -->
              </div> <!-- /.navbar-collapse -->
            </div><!--  </div> -->

          </div>

        </div>
      </div>
    </div>


    <!-- ============================================== NAVBAR-small-screen : END ============================================== -->

  </header>

