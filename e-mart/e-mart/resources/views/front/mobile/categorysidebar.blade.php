
<ul class="nav flex-column flex-nowrap overflow-hidden">
    
  
    @foreach(App\Category::where('status','1')->orderBy('position','ASC')->take(10)->get(); as $item)
        <li class="nav-item">
           
            <div class="row">
                <div class="col-6">
                    <a class="nav-link text-truncate" href="{{ $price_login == 0 || Auth::check() ? App\Helpers\CategoryUrl::getURL($item->id) : '#' }}">
                    <i class="fa {{ $item['icon'] }}"></i> 
                    <span class="d-inline">{{ $item['title'] }}</span>
                    </a>
                </div>
                @if($item->subcategory->count() > 0)
                    <div class="col-6">
                        <a class="c_icon_plus float-right collapsed nav-link text-truncate" href="#submenu{{ $item['id'] }}" data-toggle="collapse">
                            <i class="fa fa-plus-square-o"></i>
                        </a>
                    </div>
                @endif
            </div>

            <div class="collapse" id="submenu{{ $item['id'] }}" aria-expanded="false">
                <ul class="flex-column pl-2 nav">
                    @foreach($item->subcategory as $subcategory)
                        <div class="row">
                            <div class="col-6">
                                <a class="nav-link text-truncate" href="{{ $price_login == 0 || Auth::check() ? App\Helpers\SubcategoryUrl::getURL($subcategory->id) : '#' }}">
                                <i class="fa {{ $subcategory['icon'] }}"></i> 
                                <span class="d-inline">{{ $subcategory['title'] }}</span>
                                </a>
                            </div>
                            @if($subcategory->childcategory->count() > 0)
                                <div class="col-6">
                                    <a class="c_icon_plus float-right collapsed nav-link text-truncate" href="#childmenu{{ $subcategory['id'] }}" data-toggle="collapse">
                                        <i class="fa fa-plus-square-o"></i>
                                    </a>
                                </div>
                            @endif
                        </div>

                        <div class="collapse" id="childmenu{{ $subcategory['id'] }}" aria-expanded="false">
                            <ul class="flex-column nav pl-4">
                                <li class="nav-item">
                                    @foreach($subcategory->childcategory as $childcategory)
                                        <a class="nav-link p-1" href="{{ $price_login == 0 || Auth::check() ? App\Helpers\ChidCategoryUrl::getURL($childcategory->id) : '#' }}"> <i class="fa fa-star-o"></i>
                                        {{ $childcategory['title'] }} </a>
                                    @endforeach
                                </li>
                            </ul>
                        </div>

                    @endforeach
                    
                </ul>
            </div>
        </li>
    @endforeach
</ul>