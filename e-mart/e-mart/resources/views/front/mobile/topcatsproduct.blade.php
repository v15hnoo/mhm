@if($top_categories->category_ids !='')
@foreach($top_categories->category_ids as $category)
@php

    $productsquery = App\Product::query();

    if ($sellerSystem == 1) {

        $topcatproducts = $productsquery->join('users','users.id','=','products.vender_id')->select('products.*')->where('products.status','=','1')->where('products.category_id','=',$category)->orderBy('products.id','DESC')->take($top_categories->pro_limit)->get();

    } else {

        $topcatproducts = $productsquery->join('users','users.id','=','products.vender_id')->select('products.*')->where('users.role_id','=','a')->where('products.status','=','1')->where('products.category_id','=',$category)->orderBy('products.id','DESC')->take($top_categories->pro_limit)->get();

    }

@endphp

<!-- top category Products -->

@foreach($topcatproducts as $pro)
@if($pro->subvariants()->count() > 0)

@foreach($pro->subvariants as $key=> $orivar)
@if($orivar->def ==1)

@php
$var_name_count = count($orivar['main_attr_id']);

$name=array();
$var_name;
$newarr = array();
for($i = 0; $i<$var_name_count; $i++){ $var_id=$orivar['main_attr_id'][$i];
    $var_name[$i]=$orivar['main_attr_value'][$var_id]; // echo($orivar['main_attr_id'][$i]);
    $name[$i]=App\ProductAttributes::where('id',$var_id)->first();

    }


    try{
    $url =
    url('details').'/'.$pro->id.'?'.$name[0]['attr_name'].'='.$var_name[0].'&'.$name[1]['attr_name'].'='.$var_name[1];
    }catch(Exception $e)
    {
    $url = url('details').'/'.$pro->id.'?'.$name[0]['attr_name'].'='.$var_name[0];
    }

    @endphp

    <div class="col-6">
        <div class="item item-carousel">
            <div class="products">
                <div class="product">
                    <div class="product-image">
                        <div class="image {{ $orivar->stock ==0 ? "pro-img-box" : ""}}">

                            <a href="{{$url}}" title="{{$pro->name}}">

                                @if(count($pro->subvariants)>0)

                                @if(isset($orivar->variantimages['main_image']))
                                <img class="lazy {{ $orivar->stock ==0 ? "filterdimage" : ""}}"
                                    data-src="{{url('variantimages/thumbnails/'.$orivar->variantimages['main_image'])}}"
                                    alt="{{$pro->name}}">
                                <img class="lazy {{ $orivar->stock ==0 ? "filterdimage" : ""}}  hover-image"
                                    data-src="{{url('variantimages/hoverthumbnail/'.$orivar->variantimages['image2'])}}"
                                    alt="" />
                                @endif

                                @else
                                <img class="lazy {{ $orivar->stock ==0 ? "filterdimage" : ""}}"
                                    title="{{ $pro->name }}" data-src="{{url('images/no-image.png')}}"
                                    alt="No Image" />

                                @endif



                            </a>
                        </div>

                        @if($orivar->stock == 0)
                        <h6 align="center" class="oottext"><span>{{ __('staticwords.Outofstock') }}</span></h6>
                        @endif

                        @if($orivar->stock != 0 && $orivar->products->selling_start_at != null &&
                        $orivar->products->selling_start_at >= $current_date)
                        <h6 align="center" class="oottext2"><span>{{ __('staticwords.ComingSoon') }}</span></h6>
                        @endif
                        <!-- /.image -->



                        @if($pro->featured=="1")
                        <div class="tag hot"><span>{{ __('staticwords.Hot') }}</span></div>
                        @elseif($pro->offer_price!="0")
                        <div class="tag sale"><span>{{ __('staticwords.Sale') }}</span></div>
                        @else
                        <div class="tag new"><span>{{ __('staticwords.New') }}</span></div>
                        @endif
                    </div>


                    <!-- /.product-image -->

                    <div class="product-info text-left">
                        <h3 class="name"><a
                                href="{{$url}}">{{substr($pro->name, 0, 20)}}{{strlen($pro->name)>20 ? '...' : ""}}</a>
                        </h3>
                        @php
                        $reviews = ProductRating::getReview($pro);
                        @endphp

                        @if($reviews != 0)


                        <div class="pull-left">
                            <div class="star-ratings-sprite"><span style="width:<?php echo $reviews; ?>%"
                                    class="star-ratings-sprite-rating"></span></div>
                        </div>


                        @else
                        <div class="no-rating">{{'No Rating'}}</div>
                        @endif
                        <div class="description"></div>


                        <!-- Product-price -->

                        <div class="product-price"> <span class="price">

                                @if($price_login == '0' || Auth::check())

                                @php

                                $result = ProductPrice::getprice($pro, $orivar)->getData();

                                @endphp


                                @if($result->offerprice == 0)

                                <span class="price"><i class="{{session()->get('currency')['value']}}"></i>
                                    {{ sprintf("%.2f",$result->mainprice*$conversion_rate) }}</span>

                                @else

                                <span class="price"><i
                                        class="{{session()->get('currency')['value']}}"></i>{{ sprintf("%.2f",$result->offerprice*$conversion_rate) }}</span>

                                <span class="price-before-discount"><i
                                        class="{{session()->get('currency')['value']}}"></i>{{  sprintf("%.2f",$result->mainprice*$conversion_rate)  }}</span>

                                @endif

                                @endif
                        </div>

                        <!-- /.product-price -->

                    </div>
                    @if($orivar->stock != 0 && $orivar->products->selling_start_at != null &&
                    $orivar->products->selling_start_at >= $current_date)
                    @elseif($orivar->stock < 1) @else <!-- /.product-info -->
                        <div class="cart clearfix animate-effect">
                            <div class="action">
                                <ul class="list-unstyled">
                                    @if(!empty($auth))
                                    <?php $cart_table = App\Cart::where('user_id',$auth->id)->where('pro_id',$pro->id)->first(); ?>@endif
                                    @if(empty($cart_table))

                                    @if($price_login != 1 || Auth::check())
                                    <li id="addCart" class="lnk wishlist">


                                        <form method="POST"
                                            action="{{route('add.cart',['id' => $pro->id ,'variantid' =>$orivar->id, 'varprice' => $result->mainprice, 'varofferprice' => $result->offerprice ,'qty' =>$orivar->min_order_qty])}}">
                                            {{ csrf_field() }}
                                            <button title="{{ __('staticwords.AddtoCart') }}" type="submit"
                                                class="addtocartcus btn">
                                                <i class="fa fa-shopping-cart"></i>
                                            </button>
                                        </form>


                                    </li>

                                    @endif
                                    @else
                                    <li id="addCart" class="lnk wishlist"> <a class="add-to-cart"
                                            href="{{url('remove_table_cart/'.$orivar->id)}}"
                                            title="{{ __('Remove Cart') }}"> <i class="icon fa fa-times"></i> </a>
                                    </li>
                                    @endif

                                    @auth
                                    @if(Auth::user()->wishlist->count()<1) <li class="lnk wishlist">

                                        <a mainid="{{ $orivar->id }}" class="cursor-pointer add-to-cart addtowish"
                                            data-add="{{url('AddToWishList/'.$orivar->id)}}"
                                            title="{{ __('staticwords.AddToWishList') }}">
                                            <i class="icon fa fa-heart"></i>
                                        </a>

                                        </li>
                                        @else

                                        @php
                                        $ifinwishlist =
                                        App\Wishlist::where('user_id',Auth::user()->id)->where('pro_id',$orivar->id)->first();
                                        @endphp

                                        @if(!empty($ifinwishlist))
                                        <li class="lnk wishlist active">
                                            <a mainid="{{ $orivar->id }}"
                                                title="{{ __('staticwords.RemoveFromWishlist') }}"
                                                class="add-to-cart removeFrmWish active color000 cursor-pointer"
                                                data-remove="{{url('removeWishList/'.$orivar->id)}}"> <i
                                                    class="icon fa fa-heart"></i> </a>
                                        </li>
                                        @else
                                        <li class="lnk wishlist"> <a title="{{ __('staticwords.AddToWishList') }}"
                                                mainid="{{ $orivar->id }}"
                                                class="add-to-cart addtowish cursor-pointer text-white"
                                                data-add="{{url('AddToWishList/'.$orivar->id)}}"> <i
                                                    class="activeOne icon fa fa-heart"></i>
                                            </a></li>
                                        @endif

                                        @endif
                                        @endauth
                                        <li class="lnk"> <a class="add-to-cart"
                                                href="{{route('compare.product',$orivar->products->id)}}"
                                                title="{{ __('staticwords.Compare') }}"> <i class="fa fa-signal"
                                                    aria-hidden="true"></i> </a>
                                        </li>
                                </ul>
                            </div>
                            <!-- /.action -->
                        </div>
                        @endif
                        <!-- /.cart -->
                </div>
                <!-- /.product -->

            </div>
            <!-- /.products -->
        </div>
    </div>

    <!-- /.item -->
    @endif
    @endforeach
    @endif
    @endforeach

    <!-- END -->


    @endforeach
    @endif