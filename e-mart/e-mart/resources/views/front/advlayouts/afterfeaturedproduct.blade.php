@if($adv->layout == 'Single image layout')

    <div class="row">
        <div class="col-md-12 mt-2">
            <a href="
                    @if($adv->cat_id1 != '')
                        {{ App\Helpers\CategoryUrl::getURL($adv->cat_id1) }}
                    @elseif($adv->pro_id1 != '' && $adv->product1->subvariants)
                        {{ App\Helpers\ProductUrl::getURL($adv->pro_id1) }}
                    @else
                    {{ $adv->url1 }}
                    @endif">
                <img class="img-fluid lazy" data-src="{{ url('images/layoutads/'.$adv->image1) }}"
                    alt="{{ $adv->image1 }}">
            </a>
        </div>
    </div>

@endif

@if($adv->layout == 'Three Image Layout')
<div class="row mb-2">
    <div class="col-4 col-sm-4 col-xs-4 mt-2">
        <a href="
                        @if($adv->cat_id1 != '')
                          {{ App\Helpers\CategoryUrl::getURL($adv->cat_id1) }}
                        @elseif($adv->pro_id1 != '' && $adv->product1->subvariants)
                          {{ App\Helpers\ProductUrl::getURL($adv->pro_id1) }}
                        @else
                          {{ $adv->url1 }}
                        @endif">
            <img class="img-fluid lazy" data-src="{{ url('images/layoutads/'.$adv->image1) }}"
                alt="{{ $adv->image1 }}">
        </a>
    </div>
    <div class="col-4 col-sm-4 col-xs-4 mt-2">
        <a href="
                        @if($adv->cat_id2 != '')
                          {{ App\Helpers\CategoryUrl::getURL($adv->cat_id2) }}
                        @elseif($adv->pro_id2 != '' && $adv->product2->subvariants)
                          {{ App\Helpers\ProductUrl::getURL($adv->pro_id2) }}
                        @else
                          {{ $adv->url2 }}
                        @endif">
            <img class="img-fluid lazy" data-src="{{ url('images/layoutads/'.$adv->image2) }}"
                alt="{{ $adv->image2 }}">
        </a>
    </div>
    <div class="col-4 col-sm-4 col-xs-4 mt-2">
        <a href="
                        @if($adv->cat_id3 != '')
                          {{ App\Helpers\CategoryUrl::getURL($adv->cat_id3) }}
                        @elseif($adv->pro_id3 != '' && $adv->product3->subvariants)
                          {{ App\Helpers\ProductUrl::getURL($adv->pro_id3) }}
                        @else
                          {{ $adv->url3 }}
                        @endif">
            <img class="img-fluid lazy" data-src="{{ url('images/layoutads/'.$adv->image3) }}"
                alt="{{ $adv->image3 }}">
        </a>
    </div>
</div>
@endif

@if($adv->layout == 'Two equal image layout')
<div class="row">
    <div class="col-md-6 col-sm-6 mt-2">
        <a href="
                            @if($adv->cat_id1 != '')
                              {{ App\Helpers\CategoryUrl::getURL($adv->cat_id1) }}
                            @elseif($adv->pro_id1 != '' && $adv->product1->subvariants)
                              {{ App\Helpers\ProductUrl::getURL($adv->pro_id1) }}
                            @else
                              {{ $adv->url1 }}
                            @endif">
            <img class="img-fluid advertisement-img advertisement-img-one lazy"
                data-src="{{ url('images/layoutads/'.$adv->image1) }}" alt="{{ $adv->image1 }}">
        </a>
    </div>
    <div class="col-md-6 col-sm-6 mt-2">
        <a href="
                            @if($adv->cat_id2 != '')
                              {{ App\Helpers\CategoryUrl::getURL($adv->cat_id2) }}
                            @elseif($adv->pro_id2 != '' && $adv->product2->subvariants)
                              {{ App\Helpers\ProductUrl::getURL($adv->pro_id2) }}
                            @else
                              {{ $adv->url2 }}
                            @endif">
            <img class="img-fluid advertisement-img advertisement-img-one lazy"
                data-src="{{ url('images/layoutads/'.$adv->image2) }}" alt="{{ $adv->image2 }}">
        </a>
    </div>
</div>
@endif


@if($adv->layout == 'Two non equal image layout')
<div class="row">
    <div class="col-md-8 d-lg-block d-md-block d-none">
        <a href="
        @if($adv->cat_id1 != '')
          {{ App\Helpers\CategoryUrl::getURL($adv->cat_id1) }}
        @elseif($adv->pro_id1 != '' && $adv->product1->subvariants)
          {{ App\Helpers\ProductUrl::getURL($adv->pro_id1) }}
        @else
          {{ $adv->url1 }}
        @endif">
            <img class="img-fluid advertisement-img advertisement-img-one lazy advertisement-images-size-one"
                data-src="{{ url('images/layoutads/'.$adv->image1) }}" alt="{{ $adv->image1 }}">
        </a>
    </div>
    <div class="col-md-4 d-lg-block d-md-block d-none">
        <a href="
          @if($adv->cat_id2 != '')
            {{ App\Helpers\CategoryUrl::getURL($adv->cat_id2) }}
          @elseif($adv->pro_id2 != '' && $adv->product2->subvariants)
            {{ App\Helpers\ProductUrl::getURL($adv->pro_id2) }}
          @else
            {{ $adv->url2 }}
          @endif">
            <img class="img-fluid advertisement-images-size-one lazy"
                data-src="{{ url('images/layoutads/'.$adv->image2) }}" alt="{{ $adv->image2 }}">
        </a>
    </div>
</div>
@endif