require('./bootstrap');

window.Vue = require('vue');


Vue.component('hello', require('./components/Hello.vue').default);
Vue.component('world', require('./components/World.vue').default);

const app = new Vue({
    el : '#app'
});