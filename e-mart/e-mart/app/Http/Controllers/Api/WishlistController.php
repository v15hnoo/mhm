<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\WishlistCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WishlistController extends Controller
{
    public function createCollection(Request $request)
    {

        if (Auth::check()) {

            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            WishlistCollection::create([
                'name' => $request->name,
                'user_id' => Auth::user()->id,
            ]);

            return response()->json($request->name . ' collection created successfully !');

        } else {
            return response()->json('You\re not logged in !');
        }

    }

    public function listCollection()
    {

        if (!Auth::check()) {
            return response()->json('You\re not logged in !');
        }

        $collection = WishlistCollection::where('user_id', '=', Auth::user()->id)->get();

        $result[] = array(
            'url' => url('api/wishlist'),
            'collectionname' => 'Favourites',
            'iteminlist' => Auth::user()->wishlist()->count(),
            'imagepath' => url('variantimages/thumbnails/'),
            'topImages' => $this->topCollectionItemImages(0) != '' ? $this->topCollectionItemImages(0) : null,
        );

        foreach ($collection as $coll) {

            $result[] = array(
                'url' => url('api/collection/' . $coll->id),
                'collectionname' => $coll->name,
                'iteminlist' => $coll->items()->count(),
                'imagepath' => url('variantimages/thumbnails/'),
                'topImages' => $this->topCollectionItemImages($coll) != '' ? $this->topCollectionItemImages($coll) : null,
            );

        }

        return response()->json(['collection' => $result]);

    }

    public function topCollectionItemImages($coll)
    {

        $images = array();

        if ($coll != '0') {

            foreach ($coll->items->take(8) as $item) {

                if (isset($item->variant)) {

                    if (isset($item->variant->variantimages)) {

                        array_push($images, $item->variant->variantimages['main_image']);

                    }

                }

            }
        } else {

            foreach (Auth::user()->wishlist as $item) {

                if (isset($item->variant)) {

                    if (isset($item->variant->variantimages)) {

                        array_push($images, $item->variant->variantimages['main_image']);

                    }

                }

            }

        }

        shuffle($images);

        return $images;

    }

    public function listCollectionItemsByID(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'currency' => 'required|max:3|min:3',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        if (!Auth::check()) {

            return response()->json('You\re not logged in !', 422);

        }

        $collection = WishlistCollection::find($id);

        if (!$collection) {
            return response()->json('Collection not found !', 422);
        }

        $result[] = array(
            'id' => $collection->id,
            'name' => $collection->name,
            'items' => count($this->collectionItem($collection, $request->currency)) > 0 ? $this->collectionItem($collection, $request->currency) : 'No items in this collection !',
        );

        return response()->json(['data' => $result]);

    }

    public function collectionItem($collection, $currency)
    {

        $wishlistItem = array();

        foreach ($collection->items as $item) {

            $productData = new ProductController;

            $price = $productData->getprice($item->variant->products, $item->variant)->getData();

            $rating = $productData->getproductrating($item->variant->products);

            $rates = new CurrencyController;

            $rate = $rates->fetchRates($currency)->getData();

            $getvariant = new CartController;

            // Pushing value in main result

            $wishlistItem[] = array(
                'wishlistid' => $item->id,
                'productname' => $item->variant->products->name,
                'variant' => $getvariant->variantDetail($item->variant),
                'thumpath' => url('variantimages/thumbnails/'),
                'thumbnail' => $item->variant->variantimages->main_image,
                'price' => (double) sprintf('%.2f', $price->mainprice * $rate->exchange_rate),
                'offerprice' => (double) sprintf("%.2f", $price->offerprice * $rate->exchange_rate),
                'stock' => $item->variant->stock != 0 ? "In Stock" : "Out of Stock",
                'rating' => (double) $rating,
                'symbol' => $rate->symbol,
            );

        }

        return $wishlistItem;

    }
}
