<?php

namespace App\Http\Controllers\Api;

use App\AddSubVariant;
use App\Cart;
use App\Http\Controllers\Controller;
use App\ProductAttributes;
use App\ProductValues;
use App\Shipping;
use App\ShippingWeight;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    public function addToCart(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'currency' => 'required|max:3|min:3',
            'variantid' => 'required|numeric',
            'quantity' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors()]);
        }

        $item = Cart::firstWhere('variant_id', '=', $request->variantid);

        $rates = new CurrencyController;

        $rate = $rates->fetchRates($request->currency)->getData();

        $variant = AddSubVariant::find($request->variantid);

        if (!$variant) {
            return response()->json(['Variant not found !'], 422);
        }

        if ($variant->stock < 1) {
            return response()->json(['Sorry ! Item is out of stock currently !'], 422);
        }

        if ($request->quantity < $variant->min_order_qty) {
            return response()->json(['For this product you need to add atleast ' . $variant->min_order_qty . ' quantity'], 422);
        }

        if ($request->quantity > $variant->max_order_qty) {
            return response()->json(['For this product you can add maximum ' . $variant->max_order_qty . ' quantity'], 422);
        }

        if ($request->quantity > $variant->stock) {
            return response()->json(['Product stock limit reached !']);
        }

        $price = new ProductController;

        $price = $price->getprice($variant->products, $variant)->getData();

        if (isset($item)) {

            $newqty = (int) $item->qty + $request->quantity;
            $item->qty = $newqty;
            $item->price_total = (float) $price->mainprice * $newqty;
            $item->semi_total = (float) $price->offerprice * $newqty;

            $item->shipping = $this->getShipping($newqty,$variant);

            $item->updated_at = now();

            $item->save();

            return response()->json(['Product quantity updated !'], 200);

        } else {

            $cart = new Cart;
            $cart->qty = $request->quantity;
            $cart->user_id = Auth::user()->id;
            $cart->pro_id = $variant->products->id;
            $cart->variant_id = $request->variantid;
            $cart->ori_price = (float) $price->mainprice;
            $cart->ori_offer_price = (float) $price->offerprice;

            $cart->price_total = (float) $price->mainprice * $request->quantity;
            $cart->semi_total  = (float) $price->offerprice * $request->quantity;

            $cart->vender_id  = $variant->products->vender->id;
            $cart->shipping   = $this->getShipping($request->quantity,$variant);
            $cart->created_at = now();
            $cart->updated_at = now();

            $cart->save();

            return response()->json(['Item added to cart successfully !'], 200);

        }

    }

    public function getShipping($qty, $variant)
    {
        $shipping = 0;

        if ($variant->products->free_shipping == 0) {

            $free_shipping = Shipping::where('id', $variant->products->shipping_id)->first();

            if (!empty($free_shipping)) {

                if ($free_shipping->name == "Shipping Price") {

                    $weight = ShippingWeight::first();

                    $pro_weight = $variant->weight;

                    if ($weight->weight_to_0 >= $pro_weight) {

                        if ($weight->per_oq_0 == 'po') {
                            $shipping = $shipping + $weight->weight_price_0;
                        } else {
                            $shipping = $shipping + $weight->weight_price_0 * $qty;
                        }

                    } elseif ($weight->weight_to_1 >= $pro_weight) {

                        if ($weight->per_oq_1 == 'po') {
                            $shipping = $shipping + $weight->weight_price_1;
                        } else {
                            $shipping = $shipping + $weight->weight_price_1 * $qty;
                        }

                    } elseif ($weight->weight_to_2 >= $pro_weight) {

                        if ($weight->per_oq_2 == 'po') {
                            $shipping = $shipping + $weight->weight_price_2;
                        } else {
                            $shipping = $shipping + $weight->weight_price_2 * $qty;
                        }

                    } elseif ($weight->weight_to_3 >= $pro_weight) {

                        if ($weight->per_oq_3 == 'po') {
                            $shipping = $shipping + $weight->weight_price_3;
                        } else {
                            $shipping = $shipping + $weight->weight_price_3 * $qty;
                        }

                    } else {

                        if ($weight->per_oq_4 == 'po') {
                            $shipping = $shipping + $weight->weight_price_4;
                        } else {
                            $shipping = $shipping + $weight->weight_price_4 * $qty;
                        }

                    }

                } else {

                    $shipping = $shipping + $free_shipping->price;

                }
            }

        }

        return $shipping;
    }

    public function yourCart(Request $request){

        $validator = Validator::make($request->all(), [
            'currency' => 'required|max:3|min:3'
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors()]);
        }

        $rates = new CurrencyController;

        $rate = $rates->fetchRates($request->currency)->getData();
   

        if(count($this->cartproducts($request->currency))){

            $cart = array(
                'products'    =>   $this->cartproducts($request->currency),
                'subtotal'    =>   sprintf("%.2f",$this->cartTotal()->getData()->subtotal * $rate->exchange_rate),
                'shipping'    =>   sprintf("%.2f",$this->cartTotal()->getData()->shipping * $rate->exchange_rate),
                'grand_total' =>   sprintf("%.2f",$this->cartTotal()->getData()->grandTotal * $rate->exchange_rate),
                'currency'    =>   $rate->code,
                'symbol'      =>   $rate->symbol
            );

            return response()->json($cart);
        }else{
            return response()->json(['Your cart is empty !']);
        }

        

    }

    public function cartproducts($currency){

        $rates = new CurrencyController;

        $rate = $rates->fetchRates($currency)->getData();
        
        $products = array();

        foreach(Auth::user()->cart as $cart){

            $productData = new ProductController;

            $rating = $productData->getproductrating($cart->product);

            $reviews = $productData->getProductReviews($cart->product);

            $products[] = array(
                'cartid'       => $cart->id, 
                'productid'    => $cart->product->id,
                'variantid'    => $cart->variant_id,
                'productname'  => $cart->product->name,
                'mainprice'    =>  (float) sprintf("%.2f",$cart->price_total * $rate->exchange_rate),
                'offerprice'   =>  (float) sprintf("%.2f",$cart->semi_total * $rate->exchange_rate),
                'qty'          =>  $cart->qty,
                'rating'       =>  $rating,
                'review'       =>  count($reviews),
                'soldby'       =>  $cart->product->store->name,
                'variant'      =>  $this->variantDetail($cart->variant)
            );

        }

        return $products;

    }

    public function cartTotal(){

        $totalshipping = 0;
        $subtotal = 0;

        foreach(Auth::user()->cart as $cart){
            
            if($cart->semi_total != 0){

                $subtotal = $subtotal + $cart->semi_total + $cart->tax_amount;

            }else{

                $subtotal = $subtotal + $cart->price_total + $cart->tax_amount;

            }

            $totalshipping = $totalshipping + $cart->shipping;
        }


        $grandtotal = $totalshipping + $subtotal;

        return response()->json([

            'subtotal' => $subtotal,
            'grandTotal' => $grandtotal,
            'shipping' => $totalshipping

        ]);

    }

    public function variantDetail($variant){

        $varcount = count($variant->main_attr_value);
        $var_main = '';
        $i = 0;
        $othervariantName = null;

        $variants = null;

        foreach ($variant->main_attr_value as $key => $orivars) {

            $i++;

            $loopgetattrname = ProductAttributes::where('id', $key)->first();
            $getvarvalue = ProductValues::where('id', $orivars)->first();

            $result[] = array(
                'attr_id' => $loopgetattrname['id'],
                'attrribute' => $loopgetattrname['attr_name'],
            );

            if ($i < $varcount) {
                if (strcasecmp($getvarvalue->unit_value, $getvarvalue->values) != 0 && $getvarvalue->unit_value != null) {
                    if ($getvarvalue->proattr->attr_name == "Color" || $getvarvalue->proattr->attr_name == "Colour" || $getvarvalue->proattr->attr_name == "color" || $getvarvalue->proattr->attr_name == "colour") {

                        $othervariantName = $getvarvalue->values;

                    } else {
                        $othervariantName = $getvarvalue->values . $getvarvalue->unit_value;
                    }
                } else {
                    $othervariantName = $getvarvalue->values;
                }

            } else {

                if (strcasecmp($getvarvalue->unit_value, $getvarvalue->values) != 0 && $getvarvalue->unit_value != null) {

                    if ($getvarvalue->proattr->attr_name == "Color" || $getvarvalue->proattr->attr_name == "Colour" || $getvarvalue->proattr->attr_name == "color" || $getvarvalue->proattr->attr_name == "colour") {

                        $othervariantName = $getvarvalue->values;

                    } else {
                        $othervariantName = $getvarvalue->values . $getvarvalue->unit_value;
                    }

                } else {
                    $othervariantName = $getvarvalue->values;
                }

            }

            $variants[] = array(
                'var_name' => $othervariantName,
                'attr_name' => $loopgetattrname['attr_name'],
                'type' => $loopgetattrname['attr_name'] == 'color' || $loopgetattrname['attr_name'] == 'Color' || $loopgetattrname['attr_name'] == 'colour' || $loopgetattrname['attr_name'] == 'Colour' ? 'c' : 's',
            );

        }

        return $variants;

    }
    
    public function increaseQuantity(Request $request){

        $validator = Validator::make($request->all(), [
            'cartid'   => 'required|numeric',
            'currency' => 'required|max:3|min:3',
            'quantity' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors()]);
        }
        
        $cartrow = Cart::find($request->cartid);

        if(!$cartrow){
            return response()->json(['Cart item not found !'],422);
        }

        $variant = AddSubVariant::find($cartrow->variant_id);

        $newqty = (int) $cartrow->qty + $request->quantity;

        if (!$variant) {
            return response()->json(['Variant not found !'], 422);
        }

        if ($variant->stock < 1) {
            return response()->json(['Sorry ! Item is out of stock currently !'], 422);
        }

        if ($newqty > $variant->stock) {
            return response()->json(['Product stock limit reached !'],422);
        }

        if ($request->quantity < $variant->min_order_qty) {
            return response()->json(['For this product you need to add atleast ' . $variant->min_order_qty . ' quantity'], 422);
        }

        if ($request->quantity > $variant->max_order_qty) {
            return response()->json(['For this product you can add maximum ' . $variant->max_order_qty . ' quantity'], 422);
        }

      

        $price = new ProductController;

        $price = $price->getprice($variant->products, $variant)->getData();
 
        $cartrow->qty = $newqty;

        $cartrow->price_total = (float) $price->mainprice * $newqty;
        $cartrow->semi_total = (float) $price->offerprice * $newqty;

        $cartrow->shipping = $this->getShipping($newqty,$variant);

        $cartrow->updated_at = now();

        $cartrow->save();

        $rates = new CurrencyController;

        $rate = $rates->fetchRates($request->currency)->getData();

        $cart = array(
            'products'    =>   $this->cartproducts($request->currency),
            'subtotal'    =>   sprintf("%.2f",$this->cartTotal()->getData()->subtotal * $rate->exchange_rate),
            'shipping'    =>   sprintf("%.2f",$this->cartTotal()->getData()->shipping * $rate->exchange_rate),
            'grand_total' =>   sprintf("%.2f",$this->cartTotal()->getData()->grandTotal * $rate->exchange_rate),
            'currency'    =>   $rate->code,
            'symbol'      =>   $rate->symbol
        );

        return response()->json($cart, 200);
    }

    public function cartItemRemove(Request $request){

    
        if(Auth::check()){

            $validator = Validator::make($request->all(), [
                'cartid'   => 'required|numeric'
            ]);
    
            if ($validator->fails()) {
                return response()->json([$validator->errors()]);
            }

            $row = Cart::find($request->cartid);

            if(!$row){
                return response()->json(['Cart item not found !'],422);
            }

            $row->delete();

            return response()->json(['Item is removed from your cart !'],200);

        }else{
            return response()->json(['Please log in to continue.'],422);
        }

    }

    public function clearCart(){

        if(Auth::check()){

           auth()->user()->cart()->delete();

           return response()->json(['Cart is now empty !']);

        }else{
            return response()->json(['Log in to continue...']);
        }

    }


}
